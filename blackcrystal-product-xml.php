<?php
/*
Plugin Name: BlackCrystal Product XML
Plugin URI: http://www.blackcrystal.net/project/blackcrystal-product-xml/
Description: Plugin adds XML feed with products to your webshop (Woocommerce)
Version: 1.1
Author: Sergei Miami <miami@blackcrystal.net>
Author URI: http://www.blackcrystal.net

Copyright 2016  Sergei Miami <miami@blackcrystal.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

if ( !defined( 'ABSPATH' ) ) exit;

class blackcrystal_product_xml
{

	private static $instance = null;

	public static function activate()
	{
		add_rewrite_rule('^blackcrystal_product_xml/([a-zA-Z0-9-]+)\.xml?', 'index.php?blackcrystal_product_xml=$matches[1]', 'top');
		global $wp_rewrite; $wp_rewrite->flush_rules( false );
	}

	public static function init()
	{
		if( self::$instance == null ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	private function __construct()
	{
		load_plugin_textdomain('blackcrystal_product_xml');

		if ( is_admin() ) {
			add_action( 'admin_menu',   array( $this, 'admin_menu' ) );
		}
		else
		{
			add_filter( 'query_vars',   array( $this, 'query_vars' ));
			add_filter( 'request',      array( $this, 'request' ));
		}
	}

	public function query_vars( $vars )
	{
		$vars[] = 'blackcrystal_product_xml';
		return $vars;
	}


	public function request( $vars )
	{
		if ( ! isset ( $vars['blackcrystal_product_xml'] ) ) {
			return $vars;
		}

		$options = get_option( 'blackcrystal_product_xml' );
		if ( ! $options ) { $options = array(); }

		foreach ( $options as $feed ) {
			if (isset($feed['url'] ) && $feed['url'] == $vars['blackcrystal_product_xml']){
				$this->render_feed( $feed['discount'] );
			}
		}

		// error 404
		include( get_query_template( '404' ) );
		exit;
	}

	public function render_feed($discount)
	{
		header('Content-type: text/xml');
		$params = array(
			'posts_per_page' => -1,
			'post_type' => array('product', 'product_variation'),
			'post_status'=>array('publish', 'inherit'),
			'meta_query' => array(
				array(
					'key' => '_stock_status',
					'value' => 'instock'
				), array(
					'key' => '_price',
					'compare' => '>',
					'value' => '0',
				)
			)
		);

		echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
		echo '<mywebstore><products>';

		$query = new WP_Query($params);
		while($query->have_posts()) {

			$query->the_post();
			$id      = get_the_ID();
			$product = new WC_Product( $id );
			$image   = wp_get_attachment_image_url( $product->get_image_id( 'full' ) );
			$content = strip_tags( __( $product->post->post_content ) );
			$excerpt = strip_tags( __( $product->post->post_excerpt ) );
			$price   = get_post_meta( $id , 'wholesale_customer_wholesale_price' , true );
			if (!$price)
				$price   = $product->get_price();

			$price_with_discount   = round( $price * (100-$discount)/100 , 2 );

			echo <<<XML
<product>
	<id>$id</id>
	<sku>{$product->get_sku()}</sku>
	<permalink>{$product->get_permalink()}</permalink>
	<title><![CDATA[{$product->get_title()}]]></title>
	<post_content><![CDATA[{$content}]]></post_content>
	<post_excerpt><![CDATA[{$excerpt}]]></post_excerpt>
	<image>{$image}</image>
	<categories>{$product->get_categories( '' )}</categories>
	<tags>{$product->get_tags( '' )}</tags>
	<regular_price>{$product->get_price()}</regular_price>
	<wholesale_price>{$price}</wholesale_price>
	<personal_discount>{$discount}%</personal_discount>
	<personal_price>{$price_with_discount}</personal_price>
	<stock_status>{$product->stock_status}</stock_status>
	<stock_quantity>{$product->get_stock_quantity()}</stock_quantity>
</product>
XML;

		}

		wp_reset_postdata();
		echo '</products></mywebstore>';
		exit;
	}

	function admin_menu()
	{
		add_options_page('BlackCrystal Product XML', 'BlackCrystal Product XML',
			'manage_options','blackcrystal_product_xml', array($this, 'options_page'));

		add_action( 'admin_init', array($this,'admin_init') );
	}

	function options_page()
	{
		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		$options = get_option('blackcrystal_product_xml');
		if (!$options) $options = array();
		$options['new'] = array('name'=>'','discount'=>'');

		?>

		<div class="wrap">
		<h2>BlackCrystal Product XML</h2>

		<p>Here you can create feeds with products for your customers.</p>

		<form method="post" action="options.php">

			<?php settings_fields( 'blackcrystal_product_xml' ); ?>

			<table class="form-table">
				<tr>
					<th>#</th>
					<th>Secret feed name</th>
					<th>Discount(%)</th>
					<th>Link to feed</th>
				</tr>

				<?php foreach ( $options as $k=>$v): ?>
					<tr valign="top">
						<th scope="row"><?php echo is_numeric($k) ? 'Feed #'.($k+1) : 'New feed'; ?></th>
						<td><input type="text" name="blackcrystal_product_xml[<?php echo $k?>][name]" value="<?php echo esc_attr( $v['name'] ); ?>" placeholder="Feed name"/></td>
						<td><input type="text" name="blackcrystal_product_xml[<?php echo $k?>][discount]" value="<?php echo esc_attr( $v['discount'] ); ?>" placeholder="Discount"/></td>
						<td><?php if (isset($v['url'])) {
								$url = get_site_url().'?blackcrystal_product_xml='. $v['url'];
								echo "<a href='" . esc_url($url) . "' target='blackcrystal_product_xml_preview'>" . esc_html($url) . "</a>";
						} elseif (is_numeric($k)) { echo "Not active"; }?></td>
					</tr>
				<?php endforeach; ?>

			</table>

			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
			</p>

		</form>
		</div><?php
	}

	public function admin_init()
	{
		register_setting(  'blackcrystal_product_xml' , 'blackcrystal_product_xml', array($this, 'pre_save'));
	}

	public function pre_save($options)
	{

		foreach ($options as $k=>$v)
		{

			// clean empty values
			if ( strlen( $v['name'] . $v['discount'] ) === 0 ) {
				unset( $options[ $k ] );
				continue;
			}

			// activate good feeds and deactivate bad
			$url = sanitize_title($v['name']);
			$discount = $v['discount'];
			if (strlen($url) && strlen($discount) && is_numeric($discount))
				$options[$k]['url'] = $url;
			else
				unset($options[$k]['url']);
		}

		return array_values($options);
	}

}

// clean rewrite cache iss expensive thing, let's do it once on activation of plugin
// register_activation_hook( __FILE__, array( 'blackcrystal_product_xml', 'activate' ) );

// let's rock!
add_action( 'plugins_loaded', array('blackcrystal_product_xml', 'init') );